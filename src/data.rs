use anyhow::Error;
use crossbeam::queue::{ArrayQueue, PushError};
use postgres::{row::Row, types::Type, Client, Error as PGError, NoTls, Statement};
use std::collections::HashSet;
use std::sync::{atomic::Ordering, Arc};
use std::thread::sleep;
use std::time;

use super::config::{
    EventInfo, EventTypeId, PyConfiguration, RunConfiguration, RunId, TrainingSet,
    BG_PERCENT_LENGTH, ENERGY_LENGTH, LOCATION_LENGTH, NUM_DET, SPURIOUS_COINC_LENGTH_PER_INPUT,
    TYPE_ID_LENGTH_PER_INPUT, event_set_to_vec
};
use super::{BACKGROUND, SHUTDOWN_THREADS};
use ndarray::{stack, Array, Axis};

use rand::seq::SliceRandom;
use rand::{thread_rng, Rng};
use rand_distr::{Distribution, Exp, Normal};

/// A struct that contains everything a worker thread needs to perform its job.
struct WorkThreadContext {
    work_queue: Arc<ArrayQueue<RunId>>,
    res_queue: Arc<ArrayQueue<TrainingSet>>,
    cfg: RunConfiguration,
    pycfg: PyConfiguration,
    client: Client,
    statement: Statement,
    exp_distribution: Exp<f32>,
}

/// A struct for generating random events from database data and configuration in a uniform way.
/// This includes handling spurious coincidences, background randomization, gaussian spreading and
/// so on.
///
/// The lifetime exists because background is going to be a reference to the BACKGROUND singleton.
struct EventGenerator<'a, T> {
    background_chance: f32,
    spurious_coincidence_chance: f32,
    gaussian_spreader_fn: fn(EventInfo) -> EventInfo,
    background: &'a Vec<EventInfo>,
    data_iter: T,
}

/// The output data from the EventGenerator class
struct EventGeneratorOutput {
    event: EventInfo,
    type_ids: HashSet<EventTypeId>,
    is_spurious_coincidence: f32,
}

impl<'a, T> EventGenerator<'a, T>
where
    T: Iterator<Item = Row>,
{
    /// Gets the next single event, this is called whenever the next function decides it needs
    /// another event (for instance, it will call once for a normal event, but if there is a
    /// spurious coincidence it will call again.)
    fn get_next_single(&mut self) -> (HashSet<EventTypeId>, Option<EventInfo>) {
        // type_id fields are [is_background, is_signal, is_photopeak, is_mixture]
        // We cannot determine if an event is a mixture when it is at this stage, we will handle
        // that later on

        let mut event_type = HashSet::new();

        if thread_rng().gen::<f32>() < self.background_chance {
            let mut bg_event = self.background.choose(&mut thread_rng()).unwrap().clone();
            bg_event.shuffle_all();
            event_type.insert(EventTypeId::Background);
            (event_type, Some(bg_event))
        } else {
            let signal_event = self
                .data_iter
                .next()
                .and_then(|x| Some(EventInfo::from(&x)));

            event_type.insert(EventTypeId::Signal);
            signal_event.iter().for_each(|x| {
                if x.to_raw_array().iter().any(|&x| (x - 1.).abs() < 1e-5) {
                    event_type.insert(EventTypeId::Photopeak);
                }
            });

            (event_type, signal_event)
        }
    }
}

impl<'a, T> Iterator for EventGenerator<'a, T>
where
    T: Iterator<Item = Row>,
{
    type Item = EventGeneratorOutput;

    /// Implementation of the next function for the iterator interface for the EventGenerator.
    /// Gets the next event
    fn next(&mut self) -> Option<EventGeneratorOutput> {
        let mut is_spurious_coincidence = 0.;
        let (mut type_id, event) = self.get_next_single();
        let mut event_vec = match event {
            Some(e) => vec![e],
            None => return None,
        };

        while thread_rng().gen::<f32>() < self.spurious_coincidence_chance {
            is_spurious_coincidence = 1.;
            let (coinc_type_id, event) = self.get_next_single();
            event_vec.push(match event {
                Some(e) => e,
                None => break,
            });
            type_id.extend(coinc_type_id);
        }

        let event = event_vec.into_iter().sum();

        // Save the gaussian spreading for the end.  Also respreads the background (gathered from
        // experimental data), but that shouldn't be too bad, I hope.  It's better than the
        // alternative, which is spreading each event individually, then adding together, making
        // the resolution completely wrong for the combined energy
        let event = (self.gaussian_spreader_fn)(event);

        Some(EventGeneratorOutput {
            event: event,
            type_ids: type_id,
            is_spurious_coincidence: is_spurious_coincidence,
        })
    }
}

/// A function that will take an event and randomize its energy in a gaussian fashion, simulating
/// detector resolution.
fn gaussian(e: EventInfo) -> EventInfo {
    // This is adapted from a paper that investigated 2x4x16 NaI detectors.  We are erroneously
    // applying it to 2x2x8 detectors, but it's better than nothing so far.
    // https://www.pnnl.gov/main/publications/external/technical_reports/pnnl-14735.pdf
    fn stdev_nai(energy: f32) -> f32 {
        let a = 56.3 / 3000.;
        let b = 2.721;
        (a + b * energy).sqrt() * 2.3548 / (3000. as f32).sqrt()
    }

    // We assume that the sri has better resolution by a factor of about 2.  No data to support
    // this, it is just a wild guess.
    fn stdev_sri(energy: f32) -> f32 {
        stdev_nai(energy) / 2.
    }

    fn gauss_one(energy: f32, stdev: fn(f32) -> f32) -> f32 {
        let normal = Normal::new(energy, stdev(energy)).unwrap();
        normal.sample(&mut thread_rng())
    }

    EventInfo {
        d0: gauss_one(e.d0, stdev_nai),
        d1: gauss_one(e.d1, stdev_nai),
        d2: gauss_one(e.d2, stdev_nai),
        d3: gauss_one(e.d3, stdev_nai),
        d4: gauss_one(e.d4, stdev_sri),
    }
}

/// Connects to a database using the data within the RunConfiguration.
pub fn connect(cfg: &RunConfiguration) -> Result<Client, PGError> {
    // Connect to database.
    let client = postgres::Client::connect(
        &format!(
            "host={host} user={user} password='{pass}'",
            host = cfg.db_host,
            user = cfg.db_user,
            pass = cfg.db_pass,
        ),
        NoTls,
    )?;

    Ok(client)
}

/// Entry point for a "worker_thread" which takes the data from the work queue, processes it, and puts
/// it in a result queue.
pub fn worker_thread(
    work_queue: Arc<ArrayQueue<RunId>>,
    res_queue: Arc<ArrayQueue<TrainingSet>>,
    cfg: RunConfiguration,
    pycfg: PyConfiguration,
) -> Result<(), Error> {
    let mut client = connect(&cfg)?;
    let statement = prepare_event_query(&mut client, &pycfg)?;
    let bg_exp_parameter = pycfg.bg_exp_parameter;

    let mut thread_context = WorkThreadContext {
        work_queue: work_queue,
        res_queue: res_queue,
        cfg: cfg,
        pycfg: pycfg,
        client: client,
        statement: statement,
        exp_distribution: Exp::new(bg_exp_parameter).unwrap(),
    };

    loop {
        unsafe {
            if SHUTDOWN_THREADS.load(Ordering::Relaxed) {
                return Ok(());
            }
        }

        let job = thread_context.work_queue.pop();

        match job {
            Ok(val) => {
                //println!("Got Job.");
                thread_context
                    .read_one_set(val)
                    .or_else::<PGError, _>(|x| {
                        println!("Failed to read set: {}", x);
                        Ok(())
                    })
                    .unwrap(); // saves as a side effect
                               //println!("Done with job.");
            }
            Err(_) => sleep(time::Duration::from_millis(10)),
        };
    }
}

/// Reads the sets and returns a collection of RunIds
pub fn read_sets(cfg: &RunConfiguration) -> Result<Vec<RunId>, PGError> {
    let mut client = connect(cfg)?;
    let query_string = r#"SELECT runs.id,
            json_agg(
                json_build_object(
                    'distance', locations.distance,
                    'azimuthal', locations.azimuthal,
                    'polar', locations.polar,
                    'energy', source_energy.energy)
                ) sources
            FROM runs
            INNER JOIN run_location_source ON runs.id = run_location_source.run_id
            INNER JOIN sources ON run_location_source.source_id = sources.id
            INNER JOIN locations ON run_location_source.location_id = locations.id
            INNER JOIN source_energy ON sources.id = source_energy.source_id
            GROUP BY runs.id;"#;

    let train_sets: Vec<Row> = client.query(query_string, &[])?;
    println!("Read {} training sets", train_sets.len());

    Ok(train_sets.iter().map(RunId::from).collect())
}

/// Entry point for a "feeder_thread" which enqueues work requests.
pub fn feeder_thread(
    work_queue: Arc<ArrayQueue<RunId>>,
    mut run_ids: Vec<RunId>,
) -> Result<(), Error> {
    loop {
        unsafe {
            if SHUTDOWN_THREADS.load(Ordering::Relaxed) {
                return Ok(());
            }
        }

        run_ids.shuffle(&mut thread_rng());

        for run_id in run_ids.iter() {
            let mut run_id = run_id.clone();
            loop {
                unsafe {
                    if SHUTDOWN_THREADS.load(Ordering::Relaxed) {
                        return Ok(());
                    }
                }

                if work_queue.is_full() {
                    sleep(time::Duration::from_millis(10));
                } else {
                    // ArrayQueue gives us back the value if it fails to push it into the array, so if it
                    // fails save it back in the variable it was moved from and try again later
                    run_id = if let Err(PushError(run_id)) = work_queue.push(run_id) {
                        run_id
                    } else {
                        // pushed sucessfully, move to the next one
                        break;
                    };
                }
            }
        }
    }
}

/// Constructs a prepared statement the database to get the events for a given run_id
fn prepare_event_query(client: &mut Client, pycfg: &PyConfiguration) -> Result<Statement, PGError> {
    let query = format!(
        r#"SELECT d0, d1, d2, d3, d4
            FROM events WHERE run_id = $1
            ORDER BY RANDOM() LIMIT {limit};"#,
        limit = pycfg.max_timesamples
    );

    client.prepare_typed(&query, &[Type::INT4])
}

impl WorkThreadContext {
    /// This function is where the actual processing of the set occurs. Will read one set from the
    /// database, then process it.
    fn read_one_set(&mut self, run_id: RunId) -> Result<(), PGError> {
        // For now, assume only one source
        let run_data = self.client.query(&self.statement, &[&run_id.id])?;

        let distance = run_id.sources[0].distance / self.pycfg.max_norm;
        let x = distance * run_id.sources[0].polar.sin() * run_id.sources[0].azimuthal.cos();
        let y = distance * run_id.sources[0].polar.sin() * run_id.sources[0].azimuthal.sin();
        let z = distance * run_id.sources[0].polar.cos();
        let energy = run_id.sources[0].energy / self.pycfg.max_energy; // for now assume only a single energy

        // contains the number of combinations with less than or equal to the index.  Not counting 0
        // bits. It's a gross and dirty hack, because I didn't feel like generating
        // (\sum_{i=0}^n \binom{n}{i}) - 1 on the fly. I'm sorry :(
        assert!(
            NUM_DET == 5,
            "Don't forget to update the coincidence_lookup_table"
        );
        let coincidence_lookup_table = vec![0, 5, 15, 25, 30, 31];

        let coinc_len = coincidence_lookup_table[self.pycfg.coincidence_output_level];

        let mut labels_lengths = vec![
            LOCATION_LENGTH,
            ENERGY_LENGTH,
            SPURIOUS_COINC_LENGTH_PER_INPUT * self.pycfg.input_batch_size,
            BG_PERCENT_LENGTH,
            coinc_len,
            coinc_len,
        ];
        labels_lengths.extend(vec![TYPE_ID_LENGTH_PER_INPUT; self.pycfg.input_batch_size]);
        let labels_lengths = labels_lengths; // turn off mutability

        let data_lengths = vec![NUM_DET * 2; self.pycfg.input_batch_size];

        let init_set = |lengths: Vec<usize>| {
            lengths
                .iter()
                .map(|length| Array::<f32, _>::zeros((0, *length)))
                .collect::<Vec<_>>()
        };

        let mut set_labels = init_set(labels_lengths);
        let mut set_data = init_set(data_lengths);

        let mut coincidence_counts = vec![0; coinc_len];
        let mut signal_coincidence_counts = vec![0; coinc_len];

        let mut chunk_count = 1;
        let run_data_iter = run_data.into_iter();

        assert!(
            self.pycfg.spurious_coincidence_rate_low < self.pycfg.spurious_coincidence_rate_high
        );
        assert!(self.pycfg.background_percent_low < self.pycfg.background_percent_high);

        let spurious_coincidence_chance = thread_rng().gen_range(
            self.pycfg.spurious_coincidence_rate_low,
            self.pycfg.spurious_coincidence_rate_high,
        );
        let background_chance = loop {
            let chance = self.exp_distribution.sample(&mut thread_rng());
            if chance > self.pycfg.background_percent_low
                && chance < self.pycfg.background_percent_high
            {
                break chance;
            }
        };

        let mut event_iter = EventGenerator {
            background_chance: background_chance,
            spurious_coincidence_chance: spurious_coincidence_chance,
            gaussian_spreader_fn: gaussian,
            background: &BACKGROUND.get().expect("Uninitialized"),
            data_iter: run_data_iter,
        };

        //println!("Before while");
        while chunk_count < self.cfg.max_chunks {
            let chunk = event_iter
                .by_ref()
                .take(self.pycfg.input_batch_size)
                .collect::<Vec<_>>();

            if chunk.len() != self.pycfg.input_batch_size {
                break;
            }

            let mut data: Vec<Vec<f32>> = vec![];
            let mut type_ids: Vec<HashSet<EventTypeId>> = vec![];
            let mut is_spurious_coincidence: Vec<f32> = vec![];
            let bg_perc = event_iter.background_chance;

            for output in chunk.into_iter() {
                let event = output.event;
                let event_info = EventInfo::from(event);
                let type_id_contains_signal = output.type_ids.contains(&EventTypeId::Signal);

                type_ids.push(output.type_ids);
                is_spurious_coincidence.push(output.is_spurious_coincidence);

                let mut coincidence: Vec<u32> = vec![];
                let mut signal_coincidence: Vec<u32> = vec![];

                // coincidence
                for mask in 1..0x20u8 {
                    // 5 bits for 5 detectors
                    let bits_set = mask.count_ones() as usize;

                    // one detector is not coincidence
                    if bits_set > self.pycfg.coincidence_output_level {
                        continue;
                    }

                    let coinc_value = event_info.coincidence(mask) as u32;
                    coincidence.push(coinc_value);
                    signal_coincidence.push(if !type_id_contains_signal {
                        coinc_value
                    } else {
                        0
                    });
                }

                assert!(
                    coinc_len == coincidence.len(),
                    "Coincidence length unexpected {} vs {}",
                    coinc_len,
                    coincidence.len()
                );

                // to_data_array scales the signal by the energy
                data.push(event_info.to_data_array(energy));

                let update_coinc_counts = |ccounts: &mut Vec<u32>, coincs: Vec<u32>| {
                    ccounts.iter_mut().zip(&coincs).for_each(|pair| {
                        let (dest, src) = pair;
                        *dest += src;
                    });
                };

                // update the coincidence_counts
                update_coinc_counts(&mut coincidence_counts, coincidence);
                update_coinc_counts(&mut signal_coincidence_counts, signal_coincidence);
            }

            let mut labels = vec![vec![x, y, z]];
            labels.push(vec![energy]);
            labels.push(is_spurious_coincidence);
            labels.push(vec![bg_perc]);

            // This seem out of place, but we need to update this before we do coincidence
            // calculations.
            let counts_so_far = chunk_count * self.pycfg.input_batch_size;
            chunk_count += 1; // Don't forget to update this value

            let coincidence_counts = coincidence_counts
                .iter()
                .map(|value| {
                    // Normalize to a percentage of coincidence counts
                    *value as f32 / (counts_so_far as f32)
                })
                .collect::<Vec<_>>();

            let signal_coincidence_counts = signal_coincidence_counts
                .iter()
                .map(|value| {
                    // Normalize to a percentage of coincidence counts
                    *value as f32 / (counts_so_far as f32)
                })
                .collect::<Vec<_>>();

            labels.push(coincidence_counts);
            labels.push(signal_coincidence_counts);
            type_ids.into_iter().for_each(|x| labels.push(event_set_to_vec(x)));

            let into_ndarrays = |in_data: Vec<Vec<f32>>, out_data: Vec<Array<f32, _>>| {
                in_data
                    .into_iter()
                    .zip(out_data)
                    .map(|(in_data, out_data)| {
                        let data_len = in_data.len();
                        let in_data = Array::from(in_data).into_shape((1, data_len)).unwrap();
                        stack![Axis(0), out_data, in_data]
                    })
                    .collect::<Vec<_>>()
            };

            set_data = into_ndarrays(data, set_data);
            set_labels = into_ndarrays(labels, set_labels);
        }
        //println!("Done while");

        let mut res = TrainingSet {
            labels: set_labels,
            data: set_data,
        };

        loop {
            // ArrayQueue gives us back the value if it fails to push it into the array, so if it
            // fails save it back in the variable it was moved from and try again later
            res = if let Err(PushError(res)) = self.res_queue.push(res) {
                res
            } else {
                // println!("Pushed to queue at address: {:p}", self.res_queue);
                break;
            };
            sleep(time::Duration::from_millis(10));
        }

        Ok(())
    }
}
