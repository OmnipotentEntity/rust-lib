use derive_more::Add;
use ndarray::{Array, Dim};
use postgres::row::Row;
use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};
use serde_json::value::Value;
use std::iter::Sum;
use std::collections::HashSet;
use std::mem::swap;

pub static NUM_DET: usize = 5;
pub static LOCATION_LENGTH: usize = 3;
pub static ENERGY_LENGTH: usize = 1;
pub static TYPE_ID_LENGTH_PER_INPUT: usize = 3;
pub static SPURIOUS_COINC_LENGTH_PER_INPUT: usize = 1;
pub static BG_PERCENT_LENGTH: usize = 1;

/// Each run can have a vector of associated sources.  This structure contains this.
#[derive(Debug, Clone)]
pub struct RunId {
    pub id: i32,
    pub sources: Vec<Source>,
}

/// A source is characterized by it's location and energy
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Source {
    pub distance: f32,
    pub azimuthal: f32,
    pub polar: f32,
    pub energy: f32,
}

/// And event type may be one of 5 types, a background event, a signal event, a photopeak (which is
/// a signal but also has data on approximate energy), and mixtures, some containing background and
/// some containing only signal.
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum EventTypeId {
    Background,
    Signal,
    Photopeak,
}

pub fn event_set_to_vec(set: HashSet::<EventTypeId>) -> Vec<f32> {
    let mut result = vec![0.; 3];
    if set.contains(&EventTypeId::Background) {
        result[0] = 1.
    }
    if set.contains(&EventTypeId::Signal) {
        result[1] = 1.
    }
    if set.contains(&EventTypeId::Photopeak) {
        result[2] = 1.
    }

    result
}

/// An event will have 5 detectors associated with it, this structure holes that data.
#[derive(Debug, Add, Clone)]
pub struct EventInfo {
    pub d0: f32,
    pub d1: f32,
    pub d2: f32,
    pub d3: f32,
    pub d4: f32,
}

impl EventInfo {
    /// Create a new event with 0s for everything
    fn zero() -> EventInfo {
        EventInfo {
            d0: 0.,
            d1: 0.,
            d2: 0.,
            d3: 0.,
            d4: 0.,
        }
    }

    /// For certain things, like background data, it doesn't actually matter which detector gets
    /// what signal, so to promote training diversity, we can shuffle these signals around.
    pub fn shuffle_all(&mut self) -> &Self {
        // This function implements an unrolled version of the Fisher-Yates shuffle.
        // This is ugly. The unrolling is not for speed, but rather to avoid an equally ugly
        // convertion between a this and a Vec and then back.
        match thread_rng().gen_range(0, 5) {
            1 => swap(&mut self.d0, &mut self.d1),
            2 => swap(&mut self.d0, &mut self.d2),
            3 => swap(&mut self.d0, &mut self.d3),
            4 => swap(&mut self.d0, &mut self.d4),
            _ => (),
        };
        match thread_rng().gen_range(0, 4) {
            1 => swap(&mut self.d1, &mut self.d2),
            2 => swap(&mut self.d1, &mut self.d3),
            3 => swap(&mut self.d1, &mut self.d4),
            _ => (),
        };
        match thread_rng().gen_range(0, 3) {
            1 => swap(&mut self.d2, &mut self.d3),
            2 => swap(&mut self.d2, &mut self.d4),
            _ => (),
        };
        match thread_rng().gen_range(0, 2) {
            1 => swap(&mut self.d3, &mut self.d4),
            _ => (),
        };

        self
    }
}

impl Sum for EventInfo {
    /// Implements sum for EventInfo
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = EventInfo>,
    {
        let acc = EventInfo::zero();
        // uses fold instead of reduce because I couldn't convince myself that it would never be
        // the case that the EventInfo collection would be guaranteed non-empty.
        iter.fold(acc, |acc, x| acc + x)
    }
}

/// RunConfiguration contains configuration information that is not needed by python
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RunConfiguration {
    pub db_host: String,
    pub db_pass: String,
    pub db_user: String,
    pub max_chunks: usize,
    pub num_threads: usize,
    pub res_queue_size: usize,
    pub work_queue_size: usize,
}

/// PyConfiguration contains configuration information that is shared between the Rust library and
/// Python.
// Parallel definition of this structure in lstm_data.py
// struct config_t
#[derive(Debug, Clone)]
#[repr(C)]
pub struct PyConfiguration {
    pub background_percent_low: f32,
    pub background_percent_high: f32,
    pub bg_exp_parameter: f32,
    pub coincidence_output_level: usize,
    pub input_batch_size: usize,
    pub max_energy: f32,
    pub max_norm: f32,
    pub max_timesamples: u32,
    pub spurious_coincidence_rate_low: f32,
    pub spurious_coincidence_rate_high: f32,
    pub test_percentage: f32,
}

/// A TrainingSet is a the data structure that holds the data in a single Run.  Multiple Runs are
/// put together to make a batch.  labels contain the labels for each output expected from the NN,
/// while data contains a vector of each input to the NN.
pub struct TrainingSet {
    pub labels: Vec<Array<f32, Dim<[usize; 2]>>>,
    pub data: Vec<Array<f32, Dim<[usize; 2]>>>,
}

impl From<&Row> for RunId {
    /// Small utility function that converts a database row into a RunId
    fn from(row: &Row) -> Self {
        RunId {
            id: row.get(0),
            sources: row
                .get::<_, Value>(1)
                .as_array()
                .unwrap()
                .iter()
                .map(|x| Source::deserialize(x).unwrap())
                .collect(),
        }
    }
}

impl EventInfo {
    /// Returns whether or not the event features all of the requested events in the bitmask
    /// the format of the query_mask bitmask is (from MSB to LSB):
    /// 0 0 0 X X X X X
    ///       ^ ^ ^ ^ ^-D4
    ///       | | | |-D3
    ///       | | |-D2
    ///       | |-D1
    ///       |-D0
    ///
    /// Returns true if at least all of the detectors being queried were activated.
    /// Also returns true if more detectors were activated than the ones requested.
    pub fn coincidence(&self, query_mask: u8) -> bool {
        let det_mask: u8 = self.as_bitmask();

        query_mask == (det_mask & query_mask)
    }

    /// Returns the detector state as a bitmask.
    fn as_bitmask(&self) -> u8 {
        let res: u8 = ((self.d4 != 0.0) as u8)
            | (((self.d3 != 0.0) as u8) << 1)
            | (((self.d2 != 0.0) as u8) << 2)
            | (((self.d1 != 0.0) as u8) << 3)
            | (((self.d0 != 0.0) as u8) << 4);

        res
    }

    /// Converts an EventInfo into an array suitable for feeing into the NN.
    /// The boolean fields are so that a 0.001 energy signal is still clearly indicated as a
    /// signal.
    pub fn to_data_array(&self, energy: f32) -> Vec<f32> {
        let booled = |d| if d > 0.0 { 1.0 } else { 0.0 };

        vec![
            self.d0 * energy,
            booled(self.d0),
            self.d1 * energy,
            booled(self.d1),
            self.d2 * energy,
            booled(self.d2),
            self.d3 * energy,
            booled(self.d3),
            self.d4 * energy,
            booled(self.d4),
        ]
    }

    /// Converts an EventInfo into a Vec without the boolean fields, useful for iterating across.
    pub fn to_raw_array(&self) -> Vec<f32> {
        vec![self.d0, self.d1, self.d2, self.d3, self.d4]
    }
}

impl From<&Row> for EventInfo {
    /// Small utility function that converts a database row into an EventInfo
    fn from(row: &Row) -> Self {
        EventInfo {
            d0: row.get(0),
            d1: row.get(1),
            d2: row.get(2),
            d3: row.get(3),
            d4: row.get(4),
        }
    }
}

impl std::default::Default for RunConfiguration {
    /// Default RunConfiguration data
    fn default() -> Self {
        Self {
            db_host: "localhost".to_string(),
            db_pass: "dummy".to_string(),
            db_user: "dummy".to_string(),
            max_chunks: 2001,
            num_threads: 20,
            res_queue_size: 3_000,
            work_queue_size: 3_000,
        }
    }
}
