use std::cmp;
use std::io;
use std::io::Write;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc, Mutex,
};
use std::thread;
use std::thread::sleep;
use std::time;

use anyhow::Error;
use confy;
use crossbeam::queue::ArrayQueue;
use once_cell::sync::OnceCell;

mod config;
use config::{EventInfo, PyConfiguration, RunConfiguration, RunId, TrainingSet};

mod data;
use data::{connect, feeder_thread, read_sets, worker_thread};

use ndarray::{s, stack, Array, Array3, Axis, Dim, Zip};

static mut SHUTDOWN_THREADS: AtomicBool = AtomicBool::new(true);
static mut THREAD_HANDLES: OnceCell<Mutex<Vec<std::thread::JoinHandle<Result<(), Error>>>>> =
    OnceCell::new();

static TRAIN_QUEUE: OnceCell<Arc<ArrayQueue<TrainingSet>>> = OnceCell::new();
static TEST_QUEUE: OnceCell<Arc<ArrayQueue<TrainingSet>>> = OnceCell::new();

static BACKGROUND: OnceCell<Vec<EventInfo>> = OnceCell::new();

use ndarray::{AssignElem, IntoNdProducer};

/// This type is used for the callback functions used to get the pointer to write out the data
/// generated
// Parallel definiton of this callback in lstm_data.py
// typedef data_callback_fn
type DataCallbackFn = extern "C" fn(*const *const usize, usize, usize) -> *mut *mut f32;

/// This function clones elements from the first input to the second; the two producers must have
/// the same shape
fn assign_to<'a, P1, P2, A>(from: P1, to: P2)
where
    P1: IntoNdProducer<Item = &'a A>,
    P2: IntoNdProducer<Dim = P1::Dim>,
    P2::Item: AssignElem<A>,
    A: Clone + 'a,
{
    Zip::from(from).apply_assign_into(to, A::clone);
}

/// This function consumes a Vec<Vec<T>> and returns another Vec<Vec<T>> which is the original
/// transposed
fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>> {
    assert!(!v.is_empty());
    let len = v[0].len();
    let mut iters = v.into_iter().map(|n| n.into_iter()).collect::<Vec<_>>();
    (0..len)
        .map(|_| {
            iters
                .iter_mut()
                .map(|n| n.next().unwrap())
                .collect::<Vec<T>>()
        })
        .collect()
}

/// This function performs initialization the required for the rest of the library to be called
/// from python. This include setting put threads, connecting to the database, and so on.
///
/// pycfg: Configuration for the library, passed in from python.
// Parallel definition of this function in lstm_data.py
#[no_mangle]
pub extern "C" fn init(pycfg: *const PyConfiguration) -> usize {
    assert!(!pycfg.is_null());

    // Invariant: pointer is assumed to have data and be a valid PyConfiguration object.  This is
    // the caller's responsibility
    let pycfg = unsafe { (*pycfg).clone() };

    let cfg: RunConfiguration = confy::load("basic-ml").unwrap();

    // need to extract the background data here so that it only happen once

    let client = connect(&cfg);
    let query = "SELECT d0, d1, d2, d3, d4 FROM background;";

    let bg_data = client.unwrap().query(query, &[]).unwrap();

    let _ = BACKGROUND.set(bg_data.iter().map(EventInfo::from).collect());

    // Invariant: Type is atomic primitive and hence threadsafe
    unsafe {
        SHUTDOWN_THREADS.store(false, Ordering::Relaxed);
    }

    let mut thread_handles = vec![];

    let mut start_worker_threads = |work_queue: &Arc<ArrayQueue<RunId>>,
                                    res_queue: &Arc<ArrayQueue<TrainingSet>>,
                                    num_work_threads: usize| {
        thread_handles.append(
            &mut (0..num_work_threads)
                .map(|_| {
                    let t_work_queue = Arc::clone(work_queue);
                    let t_res_queue = Arc::clone(res_queue);
                    let t_cfg = cfg.clone();
                    let t_pycfg = pycfg.clone();
                    thread::spawn(move || worker_thread(t_work_queue, t_res_queue, t_cfg, t_pycfg))
                })
                .collect(),
        );
    };

    let training_work_queue = Arc::new(ArrayQueue::<RunId>::new(cfg.work_queue_size));
    let training_res_queue = Arc::new(ArrayQueue::<TrainingSet>::new(cfg.res_queue_size));

    let _ = TRAIN_QUEUE.set(Arc::clone(&training_res_queue));

    // Try to fairly allocate threads between testing and training, ensure that if there is a
    // non-zero amount of work that the thread gets at least 1 thread anyway
    // Also make sure at least 25% of threads go to each
    let num_testing_threads = cmp::max(
        if pycfg.test_percentage == 0.0 { 0 } else { 1 },
        ((1. + pycfg.test_percentage) / 2. * cfg.num_threads as f32).round() as usize,
    );
    let num_training_threads = cmp::max(
        if pycfg.test_percentage == 1.0 { 0 } else { 1 },
        ((1. + pycfg.test_percentage) / 2. * cfg.num_threads as f32).round() as usize,
    );

    start_worker_threads(
        &training_work_queue,
        &training_res_queue,
        num_training_threads,
    );

    let testing_work_queue = Arc::new(ArrayQueue::<RunId>::new(cfg.work_queue_size));
    let testing_res_queue = Arc::new(ArrayQueue::<TrainingSet>::new(cfg.res_queue_size));

    let _ = TEST_QUEUE.set(Arc::clone(&testing_res_queue));

    start_worker_threads(&testing_work_queue, &testing_res_queue, num_testing_threads);

    let sets = read_sets(&cfg).unwrap();

    let num_test_sets = (sets.len() as f32 * pycfg.test_percentage).round() as usize;
    let train_sets = sets[num_test_sets..].to_vec();
    let test_sets = sets[..num_test_sets].to_vec();

    let mut start_feeder_thread = |work_queue: &Arc<ArrayQueue<RunId>>, run_ids: Vec<RunId>| {
        thread_handles.push({
            let t_work_queue = work_queue.clone();
            thread::spawn(move || feeder_thread(t_work_queue, run_ids))
        });
    };

    start_feeder_thread(&training_work_queue, train_sets);
    start_feeder_thread(&testing_work_queue, test_sets);

    // Invariant: the only accesses to THREAD_HANDLES is from the main thread.  It is wrapped in a
    // mutex anyway for interior mutability.
    // TODO: There is certainly a better way to do this.
    let _ = unsafe { THREAD_HANDLES.set(Mutex::new(thread_handles)) };

    sets.len()
}

/// Shuts down the reader threads, feeder threads, cleans up the database (as a side effect of the
/// threads shutting down), and so on.
// Parallel definition of this function in lstm_data.py
#[no_mangle]
pub extern "C" fn shutdown() {
    // Invariant: Type is atomic primitive and hence threadsafe
    unsafe {
        SHUTDOWN_THREADS.store(true, Ordering::Relaxed);
    }

    // Invariant: the only access to THREAD_HANDLES is from the main thread.  It is wrapped in a
    // mutex anyway for interior mutability.
    // TODO: There is certainly a better way to do this.
    let mut state = unsafe { THREAD_HANDLES.get().unwrap().lock().unwrap() };

    let _ = state
        .drain(..)
        .into_iter()
        .map(thread::JoinHandle::join)
        .collect::<Vec<thread::Result<Result<(), Error>>>>();
}

/// Writes out data to pointers provided by the two callbacks
///
/// get_data_ptr = a callback that takes a numpy compatible shape (in the form of a size, data
/// pair) and returns a pointer to f32 representing the destination of the data
/// get_label_ptr = a callback that takes a numpy compatible shape (in the form of a size, data
/// pair) and returns a pointer to f32 representing the destination of the labels
/// batch_size = the size of the data batches
/// training = a bool representing whether the data returned is training or not
/// pad = a bool representing whether or not the data should be padded to full size, if false the
/// data is truncated instead.
// Parallel definition of this function in lstm_data.py
#[no_mangle]
pub extern "C" fn get_batch(
    get_data_ptr: DataCallbackFn,
    get_label_ptr: DataCallbackFn,
    batch_size: u32,
    training: bool,
    pad: bool,
) {
    let res_queue = match {
        if training {
            TRAIN_QUEUE.get()
        } else {
            TEST_QUEUE.get()
        }
    } {
        Some(ref x) => Arc::clone(&x),
        None => panic!("Must initialize before calling get_batch"),
    };

    let mut out_data: Vec<Vec<Array<f32, Dim<[usize; 2]>>>> = vec![];
    let mut out_labels: Vec<Vec<Array<f32, Dim<[usize; 2]>>>> = vec![];

    let mut max_timesamples = 0;
    let mut min_timesamples = usize::MAX;

    for i in 0..batch_size {
        print!("\rReading record {} of {}", i + 1, batch_size);
        io::stdout().flush().expect("Failed to flush");
        loop {
            let res = res_queue.pop();
            match res {
                Ok(res) => {
                    let (max_this_timesamples, min_this_timesamples) =
                        res.data
                            .iter()
                            .fold((max_timesamples, min_timesamples), |acc, x| {
                                let this_timesamples = x.shape()[0];
                                (
                                    cmp::max(acc.0, this_timesamples),
                                    cmp::min(acc.1, this_timesamples),
                                )
                            });
                    out_data.push(res.data);
                    out_labels.push(res.labels);
                    max_timesamples = cmp::max(max_timesamples, max_this_timesamples);
                    min_timesamples = cmp::min(min_timesamples, min_this_timesamples);
                    break;
                }
                Err(_) => {
                    sleep(time::Duration::from_millis(10));
                }
            }
        }
    }

    let pad_and_stack = |data: Vec<Array<f32, Dim<[usize; 2]>>>| {
        let features = data[0].shape()[1];
        let mut res = Array3::maybe_uninit((data.len(), max_timesamples, features));
        for (i, row) in data.iter().enumerate() {
            let this_timesamples = row.shape()[0];
            let missing_timesamples = max_timesamples - this_timesamples;
            assign_to(
                stack(
                    Axis(0),
                    &[
                        row.view(),
                        Array::<f32, _>::zeros((missing_timesamples, features)).view(),
                    ],
                )
                .unwrap()
                .view(),
                res.slice_mut(s![i, .., ..]),
            );
        }

        // Invariant: We may assume this is initialized because we just initialized it.
        unsafe { res.assume_init() }
    };

    let chop_and_stack = |data: Vec<Array<f32, Dim<[usize; 2]>>>| {
        let features = data[0].shape()[1];
        let mut res = Array3::maybe_uninit((data.len(), min_timesamples, features));
        for (i, row) in data.iter().enumerate() {
            assign_to(
                row.slice(s![..min_timesamples, ..]),
                res.slice_mut(s![i, .., ..]),
            );
        }

        // Invariant: We may assume this is initialized because we just initialized it.
        unsafe { res.assume_init() }
    };

    let stack_data = |out: Vec<Vec<Array<f32, Dim<[usize; 2]>>>>| {
        out.into_iter()
            .map(|data| {
                if pad {
                    pad_and_stack(data)
                } else {
                    chop_and_stack(data)
                }
            })
            .collect::<Vec<_>>()
    };

    let out_data = stack_data(transpose(out_data));
    let out_labels = stack_data(transpose(out_labels));

    let copy_to_out = |data: Vec<Array<f32, Dim<[usize; 3]>>>, get_ptr: DataCallbackFn| {
        let data_shape_ptrs = data
            .iter()
            .map(|line| line.shape().as_ptr())
            .collect::<Vec<_>>();
        let out_ptr = get_ptr(
            data_shape_ptrs.as_ptr(),
            data_shape_ptrs.len(),
            data[0].shape().len(),
        );

        let out_line: &mut [*mut f32] =
            // Invariant: It is assumed that the array of arrays is properly formatted and the
            // sizes are correct.  It is the caller's responsibility to ensure this is true.
            unsafe { std::slice::from_raw_parts_mut(out_ptr, data.len()) };

        data.iter().zip(out_line).for_each(|(line, out_line)| {
            let line_shape = line.shape();
            let line_size = line_shape.iter().product();
            let line = line.as_slice().unwrap();

            let out_line: &mut [f32] =
                // Invariant: It is assumed that the array of arrays is properly formatted and
                // the sizes are correct.  It is the caller's responsibility to ensure this is
                // true.
                unsafe { std::slice::from_raw_parts_mut(*out_line, line_size) };

            // The passed in array should be initialize to 0
            // if it is not, then it is likely that there was a mistake in passing the data in
            assert!(!out_line.iter().any(|x| *x != 0.));

            out_line.copy_from_slice(line);
        });
    };

    copy_to_out(out_data, get_data_ptr);
    copy_to_out(out_labels, get_label_ptr);
}
